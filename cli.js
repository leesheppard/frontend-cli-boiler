#!/usr/bin/env node
'use strict';
var meow = require('meow');
var boiler = require('./');

var cli = meow([
	'Usage',
	'  $ boiler [type]',
	'',
	'Examples',
	'  $ boiler min',
	'<!doctype html>',
	'<html class="no-js" lang="en">',
	'<head>',
	'<meta charset="utf-8">',
	'<meta http-equiv="x-ua-compatible" content="ie=edge">',
	'<title> </title>',
	'<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">',
	'<link rel="stylesheet" href="css/main.css">',
	'</head>',
	'<body>',
	'<script src="js/main.js"></script>',
	'</body>',
	'</html>'
]);

boiler(cli.input[0]).then(console.log).catch(console.error);
