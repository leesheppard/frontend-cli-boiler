# Frontend CLI Boiler 
Command line interface for front end boilerplates.


## Install

```
$ npm install --save frontend-cli-boiler
```

## CLI

```
$ npm install --global frontend-cli-boiler
```

```
$ boiler --help

  Command line interface for front end boilerplates.

  Usage
    $ boiler [plate]

  Examples
    $ boiler min

	<!doctype html>
	<html class="no-js" lang="en">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title> </title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/main.css">
	</head>
	<body>

	<script src="js/main.js"></script>
	</body>
	</html>
```

P.S: `plate` can be any of: min, meta, link, fav, og, fbia, twitter, g+, oembed, safari, android, chrome, ie.

## License

MIT 
